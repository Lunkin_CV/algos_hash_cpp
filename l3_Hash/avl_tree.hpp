#ifndef avl_tree_h
#define avl_tree_h

/*####################### AVLTreeNode #######################*/
template <typename TNodeInfo>
struct AVLTreeNode {
    TNodeInfo info_;
    long long int index_;
    int balance_;
    struct AVLTreeNode<TNodeInfo>* left_;
    struct AVLTreeNode<TNodeInfo>* right_;
};

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RotateRight(AVLTreeNode<TNodeInfo>* current) {
    AVLTreeNode<TNodeInfo>* pivot = current->left_;
    current->left_ = pivot->right_;
    pivot->right_ = current;
    if (pivot->balance_ == 1) {
        pivot->balance_ = 0;
        current->balance_ = 0;
    } else if (pivot->balance_ == 0){
        pivot->balance_ = -1;
        current->balance_ = 1;
    } else {
        std::string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return pivot;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RotateLeft(AVLTreeNode<TNodeInfo>* current) {
    AVLTreeNode<TNodeInfo>* pivot = current->right_;
    current->right_ = pivot->left_;
    pivot->left_ = current;
    if (pivot->balance_ == -1) {
        pivot->balance_ = 0;
        current->balance_ = 0;
    } else if (pivot->balance_ == 0){
        pivot->balance_ = 1;
        current->balance_ = -1;
    } else {
        std::string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return pivot;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RotateRightBig(AVLTreeNode<TNodeInfo>* current) {
    AVLTreeNode<TNodeInfo>* pivot = current->left_;
    AVLTreeNode<TNodeInfo>* bottom = pivot->right_;
    current->left_ = bottom->right_;
    pivot->right_ = bottom->left_;
    bottom->left_ = pivot;
    bottom->right_ = current;
    if (bottom->balance_ == 0) {
        pivot->balance_ = 0;
        current->balance_ = 0;
    } else if (bottom->balance_ == -1){
        bottom->balance_ = 0;
        pivot->balance_ = 1;
        current->balance_ = 0;
    } else if (bottom->balance_ == 1){
        bottom->balance_ = 0;
        pivot->balance_ = 0;
        current->balance_ = -1;
    } else {
        std::string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return bottom;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RotateLeftBig(AVLTreeNode<TNodeInfo>* current) {
    AVLTreeNode<TNodeInfo>* pivot = current->right_;
    AVLTreeNode<TNodeInfo>* bottom = pivot->left_;
    current->right_ = bottom->left_;
    pivot->left_ = bottom->right_;
    bottom->left_ = current;
    bottom->right_ = pivot;
    if (bottom->balance_ == 0) {
        pivot->balance_ = 0;
        current->balance_ = 0;
    } else if (bottom->balance_ == -1){
        bottom->balance_ = 0;
        pivot->balance_ = 0;
        current->balance_ = 1;
    } else if (bottom->balance_ == 1){
        bottom->balance_ = 0;
        pivot->balance_ = -1;
        current->balance_ = 0;
    } else {
        std::string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return bottom;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* Rebalance(AVLTreeNode<TNodeInfo>* node) {
    if (node->balance_ == 1 || node->balance_ == -1 || node->balance_ == 0) {
        return node;
    } else if (node->balance_ == 2) {
        if (node->left_->balance_ == 1 || node->left_->balance_ == 0) {
            return RotateRight(node);
        } else if (node->left_->balance_ == -1) {
            return RotateRightBig(node);
        }
    } else if (node->balance_ == -2) {
        if (node->right_->balance_ == -1 || node->right_->balance_ == 0) {
            return RotateLeft(node);
        } else if (node->right_->balance_ == 1) {
            return RotateLeftBig(node);
        }
    } else {
        std::string er = "Unknown balance case in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return node;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* DeleteTree(AVLTreeNode<TNodeInfo>* node) {
    if (node == nullptr) {
        std::string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (node->left_ != nullptr) {
        DeleteTree(node->left_);
    }
    if (node->right_ != nullptr) {
        DeleteTree(node->right_);
    }
    delete node;
    return nullptr;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* Insert(AVLTreeNode<TNodeInfo>* root, TNodeInfo info, long long int index, bool* need_correction) {
    bool current_need_correction = false;
    AVLTreeNode<TNodeInfo>* new_tree;
    if (root == nullptr) {
        try {
            new_tree = new struct AVLTreeNode<TNodeInfo>;
        } catch (std::bad_alloc& ba) {
            std::string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        new_tree->info_ = info;
        new_tree->index_ = index;
        new_tree->balance_ = 0;
        new_tree->right_ = new_tree->left_ = nullptr;
        *need_correction = true;
        return new_tree;
    } else if (info < root->info_) {
        IncrIndex(root);
        root->left_ = Insert(root->left_, info, root->index_ - 1, &current_need_correction);
        if (current_need_correction == true) {
            root->balance_++;
        }
        new_tree = Rebalance(root);
        if (new_tree->balance_ == 0 || current_need_correction == false) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    } else {
        root->right_ = Insert(root->right_, info, root->index_ + 1, &current_need_correction);
        if (current_need_correction == true) {
            root->balance_--;
        }
        new_tree = Rebalance(root);
        if (new_tree->balance_ == 0 || current_need_correction == false) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    }
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RemoveNode(AVLTreeNode<TNodeInfo>* root, long long int index, bool* need_correction) {
    bool current_need_correction = false;
    AVLTreeNode<TNodeInfo>* new_tree;
    if (index < root->index_) {
        DecrIndex(root);
        root->left_ = RemoveNode(root->left_, index, &current_need_correction);
        if (current_need_correction == true) {
            root->balance_--;
        }
        new_tree = Rebalance(root);
        if (new_tree->balance_ == -1 || current_need_correction == false) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    } else if (index > root->index_) {
        root->right_ = RemoveNode(root->right_, index, &current_need_correction);
        if (current_need_correction == true) {
            root->balance_++;
        }
        new_tree = Rebalance(root);
        if (new_tree->balance_ == 1 || current_need_correction == false) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    } else {
        AVLTreeNode<TNodeInfo>* left_sub = root->left_;
        AVLTreeNode<TNodeInfo>* right_sub = root->right_;
        delete root;
        if (right_sub == nullptr) {
            *need_correction = true;
            return left_sub;
        }
        AVLTreeNode<TNodeInfo>* min = FindMin(right_sub);
        min->right_ = RemoveMin(right_sub, &current_need_correction);
        min->left_ = left_sub;
        min->index_--;
        if (current_need_correction == true) {
            min->balance_++;
        }
        new_tree = Rebalance(min);
        if (current_need_correction == false || new_tree->balance_ == 1) {
            *need_correction = false;
        } else {
            *need_correction = true;
        }
        return new_tree;
    }
}

template <typename TNodeInfo>
int GetHeight(AVLTreeNode<TNodeInfo>* root)
{
    if (root == nullptr) {
        return 0;
    } else {
        int right_h = GetHeight(root->right_);
        int left_h = GetHeight(root->left_);
        return 1 + (right_h > left_h ? right_h : left_h);
    }
}

template <typename TNodeInfo>
bool BalanceCheck(AVLTreeNode<TNodeInfo>* root){
    if (root == nullptr) {
        return true;
    } else {
        if (abs(root->balance_) > 1) {
            return false;
        }
        if (GetHeight(root->left_) - GetHeight(root->right_) != root->balance_) {
            return false;
        }
        return true && BalanceCheck(root->right_) && BalanceCheck(root->left_);
    }
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* FindMin(AVLTreeNode<TNodeInfo>* node) {
    if (node == nullptr) {
        return node;
    }
    AVLTreeNode<TNodeInfo>* tmp = node;
    while (tmp->left_ != nullptr) {
        tmp = tmp->left_;
    }
    return tmp;
}

template <typename TNodeInfo>
AVLTreeNode<TNodeInfo>* RemoveMin(AVLTreeNode<TNodeInfo>* node, bool* need_correction) {
    bool current_need_correction = false;
    if(node->left_ == nullptr) {
        *need_correction = true;
        return node->right_;
    }
    
    DecrIndex(node);
    node->left_ = RemoveMin(node->left_, &current_need_correction);
    if (current_need_correction == true) {
        node->balance_--;
    }
    AVLTreeNode<TNodeInfo>* new_tree = Rebalance(node);
    if (current_need_correction == false || new_tree->balance_ == -1) {
        *need_correction = false;
    } else {
        *need_correction = true;
    }
    return new_tree;
}

/*####################### AVLBinaryTree #######################*/
template <typename TElement>
class AVLBinaryTree: public IDictionary<TElement, TElement> {
public:
    AVLBinaryTree(TElement elem);
    AVLBinaryTree() {};
    virtual ~AVLBinaryTree();
    long long int GetLength();
    int GetIsEmpty();
    TElement GetInd(long long int index);
    virtual TElement Get(TElement elem);
    long long int IndexOf(TElement elem);
    TElement GetFirst();
    TElement GetLast();
    AVLBinaryTree<TElement> GetSubsequence(long long int start_index, long long int end_index);
    void Add(TElement elem);
    virtual void Add(TElement key, TElement elem) {Add(elem);}
    long long int RemoveElem(TElement elem);
    void RemoveInd(long long int del_index);
    virtual void Remove(TElement elem);
    virtual bool GetContainsKey(TElement elem);
    virtual long long int GetCount() {return GetLength();}
    virtual long long int GetCapacity() {return GetLength();}
#ifdef DEBUG_MODE
    bool Check();
    AVLTreeNode<TElement>* GetRootPtr();
#endif
private:
    AVLTreeNode<TElement>* root_ = nullptr;
    long long int length_ = 0;
};

#ifdef DEBUG_MODE
template <typename TElement>
bool AVLBinaryTree<TElement>::Check() {
    return BalanceCheck(root_);
}
#endif

template <typename TElement>
TElement AVLBinaryTree<TElement>::Get(TElement elem) {
    if (IndexOf(elem) < 0) {
        string er = "elem not found in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return elem;
}

template <typename TElement>
bool AVLBinaryTree<TElement>::GetContainsKey(TElement elem) {
    if (IndexOf(elem) < 0) {
        return false;
    }
    return true;
}

template <typename TElement>
void AVLBinaryTree<TElement>::Remove(TElement elem) {
    if (IndexOf(elem) < 0) {
        string er = "element not found in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    RemoveElem(elem);
}

template <typename TElement>
AVLBinaryTree<TElement>::AVLBinaryTree(TElement elem) {
    try {
        root_ = new struct AVLTreeNode<TElement>;
    } catch (std::bad_alloc& ba) {
        std::string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    root_->index_ = 0;
    root_->info_ = elem;
    root_->balance_ = 0;
    root_->left_ = root_->right_ = nullptr;
    length_ = 1;
}

template <typename TElement>
AVLBinaryTree<TElement>::~AVLBinaryTree() {
    if (!GetIsEmpty()) {
        root_ = DeleteTree(root_);
    }
}

template <typename TElement>
long long int AVLBinaryTree<TElement>::GetLength() {
    return length_;
}

template <typename TElement>
int AVLBinaryTree<TElement>::GetIsEmpty() {
    if (length_ == 0) {
        return 1;
    }
    return 0;
}

template <typename TElement>
TElement AVLBinaryTree<TElement>::GetInd(long long int index) {
    if (index < 0) {
        std::string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (index >= length_) {
        std::string er = "index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    AVLTreeNode<TElement>* tmp = root_;
    while (tmp->index_ != index) {
        if (index < tmp->index_) {
            tmp = tmp->left_;
        } else {
            tmp = tmp->right_;
        }
        if (tmp == nullptr) {
            std::string er = "NULL pointer reached in ";
            er += __PRETTY_FUNCTION__;
            throw invalid_argument(er);
        }
    }
    
    return tmp->info_;
}

template <typename TElement>
long long int AVLBinaryTree<TElement>::IndexOf(TElement elem) {
    if (GetIsEmpty() == 1) {
        return -1;
    }
    struct AVLTreeNode<TElement>* tmp = root_;
    while (tmp != nullptr && tmp->info_ != elem) {
        if (elem < tmp->info_) {
            tmp = tmp->left_;
        } else {
            tmp = tmp->right_;
        }
    }
    
    if (tmp == nullptr) {
        return -1;
    }
    
    return tmp->index_;
}

template <typename TElement>
TElement AVLBinaryTree<TElement>::GetFirst() {
    return GetInd(0);
}

template <typename TElement>
TElement AVLBinaryTree<TElement>::GetLast() {
    return GetInd(length_ - 1);
}

template <typename TElement>
AVLBinaryTree<TElement> AVLBinaryTree<TElement>::GetSubsequence(long long int start_index,
                                                                long long int end_index) {
    if (start_index < 0 || end_index < 0) {
        std::string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (start_index > end_index) {
        std::string er = "start_index < end_index in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (start_index >= length_) {
        std::string er = "start_index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    long long int correct_end_index = (end_index < length_) ? end_index : (length_ - 1);
    AVLBinaryTree<TElement> tmp_tree;
    for (long long int i = start_index; i <= correct_end_index; i++) {
        tmp_tree.Add(this->GetInd(i));
    }
    return tmp_tree;
}

template <typename TElement>
void AVLBinaryTree<TElement>::Add(TElement elem) {
    bool need_correction = false;
    root_ = Insert(root_, elem, 0, &need_correction);
    length_++;
}

/*!
 * @discussion it will place left subtree instead of removed, right subtree will be added to left
 */
template <typename TElement>
void AVLBinaryTree<TElement>::RemoveInd(long long int del_index) {
    if (del_index < 0) {
        std::string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (del_index >= length_) {
        std::string er = "del_index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    bool current_need_correction = false;
    root_ = RemoveNode(root_, del_index, &current_need_correction);
    length_--;
}

/*!
 * @discussion index of element to delete is undefined
 * @return -1 if elem is not in the tree; index of deleted element in the other case
 */
template <typename TElement>
long long int AVLBinaryTree<TElement>::RemoveElem(TElement elem){
    long long int index = IndexOf(elem);
    if (index >= 0) {
        RemoveInd(index);
    }
    return index;
}

#ifdef DEBUG_MODE
template <typename TElement>
struct AVLTreeNode<TElement>* AVLBinaryTree<TElement>::GetRootPtr(){
    return root_;
}
#endif

#endif /* avl_tree_h */
