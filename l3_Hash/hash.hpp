#ifndef hash_hpp
#define hash_hpp

#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <string>
#include <stack>
#include "tree_utility.hpp"
#include "IDictionary.hpp"

using namespace std;

#define TABLE_SIZE 256

template <typename TKey>
long long int GetHashCode(TKey key) {
    if (key.empty() == 1) {
        return 0;
    }
    unsigned char h;
    long long int hash = 0;
    const int TABLE[TABLE_SIZE] = {
        228, 235, 107, 137, 212, 230,  13, 141, 229, 112,  71, 197,  52, 116,  94, 209,
        131,  88,  40, 124,  16, 244, 218, 162, 169, 100,   8, 239,   7,  10,  81,  17,
        175, 190, 151, 121, 142, 129,  45,  43, 246, 186, 132, 161,  86, 159, 206, 171,
        165,   3,  41,  22, 144, 181,  28,  21,  84,  89,  63, 250,  77, 240, 133, 128,
          6, 192, 130,  39,  65,  19,  69,  53, 104, 178, 152, 234, 127,  80, 242, 156,
        174,   0, 252,  99, 158, 150, 210, 106, 108, 153, 253,  32,  73, 198,  68, 143,
         34,  47,  29, 219,  25,  55,  51,  15, 189,   5,  67, 134, 125, 195, 163,  96,
         20, 245, 111, 193,  85, 247,  14, 194,  31, 254, 255,  44, 201,  92, 105, 236,
        170, 147, 120, 227, 136, 113, 139,  66,  48,  57,  54,  50, 202, 248, 217, 126,
         59,   9, 154, 101, 117, 168, 179,  91, 182, 221, 223, 200, 241, 118,  30,  79,
        103, 216, 251, 148, 123,  46,  97, 204,  93, 122, 155, 138,  49, 249, 157, 220,
        102, 172,  76, 199, 115,  58, 196,  82, 140,  61, 208, 225, 215, 233, 167,  42,
         38,  18,  83, 187, 183, 211, 203,  26,  24,  35, 188, 213, 185,  74, 224,  64,
        243, 110, 207, 145, 149,  60,  98, 231, 180, 146,   1, 119, 177, 205,  36, 226,
         90,  37, 184,  95, 160,  23, 237,  87, 238, 232,   4,  56,  27, 135,  75,  11,
        114,   2,  70, 176,  72, 164, 109, 166, 173,  12,  78,  33, 222, 214,  62, 191
    };
    for (int j = 0; j < 8; j++) {
        h = TABLE[(key[0] + j) % 256];
        for (long long int i = 1; i < key.length(); i++) {
            h = TABLE[h ^ key[i]];
        }
        hash += (long long int)abs((int)h) << 8 * j;
    }
    return llabs(hash);
}

#define STATE_FREE 0
#define STATE_BUSY 1
#define STATE_DEL 2

template <typename TKey, typename TElement>
struct HashTableCell {
    NodeInfo<TKey, TElement> cell;
    int state = STATE_FREE;
};

/*!
 * @discussion default properties: capacity_ = capacity_ = 10, ratio_ = 2, rarity_ = 10;
 */
template <typename TKey, typename TElement>
class HashTable:public IDictionary<TKey, TElement> {
public:
    HashTable();
    HashTable(double rarity, double ratio);
    virtual ~HashTable();
    virtual long long int GetCount();
    virtual long long int GetCapacity();
    virtual TElement Get(TKey key);
    virtual bool GetContainsKey(TKey key);
    virtual void Add(TKey key, TElement elem);
    virtual void Remove(TKey key);
    double GetRatio();
    double GetRarity();
private:
    long long int GetHash(TKey key);
    void ChangeCapacity(double ratio);
    void Extend();
    void Compress();
    
    long long int count_ = 0;
    long long int capacity_ = 10;
    double ratio_ = 2;
    double rarity_ = 10;
    struct HashTableCell<TKey, TElement>* table_ = nullptr;
};

template <typename TKey, typename TElement>
void HashTable<TKey, TElement>::ChangeCapacity(double ratio) {
    if (count_ > ratio * capacity_) {
        std::string er = "wrong ratio in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (ratio == 1) {
        return;
    }
    
    long long int old_capacity = capacity_;
    struct HashTableCell<TKey, TElement>* old_table = table_;
    
    capacity_ = (long long int)(capacity_ * ratio);
    if (capacity_ == old_capacity) {
        if (ratio < 1) {
            capacity_--;
        } else {
            capacity_++;
        }
    }
    if (capacity_ <= 0) {
        capacity_ = 1;
    }
    try {
        table_ = new HashTableCell<TKey, TElement> [capacity_];
    } catch (std::bad_alloc& ba) {
        std::string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    
    count_ = 0;
    for (long long int i = 0; i < old_capacity; i++) {
        if (old_table[i].state == STATE_BUSY) {
            Add(old_table[i].cell.GetKey(), old_table[i].cell.GetValue());
        }
    }
    
    delete [] old_table;
}

template <typename TKey, typename TElement>
void HashTable<TKey, TElement>::Extend() {
    if (count_ == capacity_) {
        ChangeCapacity(ratio_);
    }
}

template <typename TKey, typename TElement>
void HashTable<TKey, TElement>::Compress() {
    if (count_ <= capacity_ / rarity_) {
        ChangeCapacity(1 / ratio_);
    }
}

template <typename TKey, typename TElement>
long long int HashTable<TKey, TElement>::GetHash(TKey key) {
    if (capacity_ <= 0) {
        std::string er = "negative capacity_ in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return GetHashCode(key) % capacity_;
}

template <typename TKey, typename TElement>
HashTable<TKey, TElement>::HashTable() {
    try {
        table_ = new HashTableCell<TKey, TElement> [capacity_];
    } catch (std::bad_alloc& ba) {
        std::string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
}

template <typename TKey, typename TElement>
HashTable<TKey, TElement>::HashTable(double rarity, double ratio) {
    if (ratio <= 1) {
        std::string er = "wrong ratio in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (rarity < ratio) {
        std::string er = "ratio is greater than rarity in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    capacity_ = rarity + 1;
    rarity_ = rarity;
    ratio_ = ratio;
    try {
        table_ = new HashTableCell<TKey, TElement> [capacity_];
    } catch (std::bad_alloc& ba) {
        std::string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
}

template <typename TKey, typename TElement>
HashTable<TKey, TElement>::~HashTable() {
    delete [] table_;
}

template <typename TKey, typename TElement>
long long int HashTable<TKey, TElement>::GetCount() {
    return count_;
}

template <typename TKey, typename TElement>
double HashTable<TKey, TElement>::GetRatio() {
    return ratio_;
}

template <typename TKey, typename TElement>
double HashTable<TKey, TElement>::GetRarity() {
    return rarity_;
}

template <typename TKey, typename TElement>
long long int HashTable<TKey, TElement>::GetCapacity() {
    return capacity_;
}

template <typename TKey, typename TElement>
void HashTable<TKey, TElement>::Add(TKey key, TElement elem) {
    if (capacity_ == 0) {
        std::string er = "capacity == 0 in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    
    long long int index = GetHash(key);
    long long int i = index;
    while (i < capacity_) {
        if (table_[i].state == STATE_FREE || table_[i].state == STATE_DEL) {
            table_[i].cell = NodeInfo<TKey, TElement>(key, elem);
            table_[i].state = STATE_BUSY;
            count_++;
            Extend();
            return;
        }
        i++;
    }
    i = 0;
    while (i < index) {
        if (table_[i].state == STATE_FREE || table_[i].state == STATE_DEL) {
            table_[i].cell = NodeInfo<TKey, TElement>(key, elem);
            table_[i].state = STATE_BUSY;
            count_++;
            Extend();
            return;
        }
        i++;
    }
    
    std::string er = "no place for element in ";
    er += __PRETTY_FUNCTION__;
    throw runtime_error(er);
}

template <typename TKey, typename TElement>
bool HashTable<TKey, TElement>::GetContainsKey(TKey key) {
    long long int index = GetHash(key);
    long long int i = index;
    while (i < capacity_) {
        if (table_[i].state == STATE_FREE) {
            return false;
        } else if (table_[i].state == STATE_BUSY && table_[i].cell.GetKey() == key) {
            return true;
        }
        i++;
    }
    i = 0;
    while (i < index) {
        if (table_[i].state == STATE_FREE) {
            return false;
        } else if (table_[i].state == STATE_BUSY && table_[i].cell.GetKey() == key) {
            return true;
        }
        i++;
    }
    return false;
}

template <typename TKey, typename TElement>
TElement HashTable<TKey, TElement>::Get(TKey key) {
    long long int index = GetHash(key);
    long long int i = index;
    while (i < capacity_) {
        if (table_[i].state == STATE_FREE) {
            std::string er = "key not found in table in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        } else if (table_[i].state == STATE_BUSY && table_[i].cell.GetKey() == key) {
            return table_[i].cell.GetValue();
        }
        i++;
    }
    i = 0;
    while (i < index) {
        if (table_[i].state == STATE_FREE) {
            std::string er = "key not found in table in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        } else if (table_[i].state == STATE_BUSY && table_[i].cell.GetKey() == key) {
            return table_[i].cell.GetValue();
        }
        i++;
    }
    std::string er = "key not found in table in ";
    er += __PRETTY_FUNCTION__;
    throw runtime_error(er);
}

template <typename TKey, typename TElement>
void HashTable<TKey, TElement>::Remove(TKey key) {
    long long int index = GetHash(key);
    long long int i = index;
    while (i < capacity_) {
        if (table_[i].state == STATE_FREE) {
            std::string er = "key not found in table in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        } else if (table_[i].state == STATE_BUSY && table_[i].cell.GetKey() == key) {
            if (i == capacity_ - 1) {
                if (table_[0].state == STATE_FREE) {
                    table_[i].state = STATE_FREE;
                } else {
                    table_[i].state = STATE_DEL;
                }
            } else {
                if (table_[i + 1].state == STATE_FREE) {
                    table_[i].state = STATE_FREE;
                } else {
                    table_[i].state = STATE_DEL;
                }
            }
            count_--;
            Compress();
            return;
        }
        i++;
    }
    i = 0;
    while (i < index) {
        if (table_[i].state == STATE_FREE) {
            std::string er = "key not found in table in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        } else if (table_[i].state == STATE_BUSY && table_[i].cell.GetKey() == key) {
            if (table_[i + 1].state == STATE_FREE) {
                table_[i].state = STATE_FREE;
            } else {
                table_[i].state = STATE_DEL;
            }
            count_--;
            Compress();
            return;
        }
        i++;
    }
    std::string er = "key not found in table in ";
    er += __PRETTY_FUNCTION__;
    throw runtime_error(er);
}

#endif /* hash_hpp */
