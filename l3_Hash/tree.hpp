#ifndef tree_h
#define tree_h


/*####################### TreeNode #######################*/
template <typename TNodeInfo>
struct TreeNode
{
    TNodeInfo info_;
    long long int index_;
    struct TreeNode<TNodeInfo>* left_;
    struct TreeNode<TNodeInfo>* right_;
};

/*####################### BinaryTree #######################*/
template <typename TElement>
class BinaryTree:public IDictionary<TElement, TElement> {
public:
    BinaryTree(TElement elem);
    BinaryTree() {};
    virtual ~BinaryTree();
    long long int GetLength();
    int GetIsEmpty();
    TElement GetInd(long long int index);
    virtual TElement Get(TElement elem);
    long long int IndexOf(TElement elem);
    TElement GetFirst();
    TElement GetLast();
    BinaryTree<TElement> GetSubsequence(long long int start_index, long long int end_index);
    void Add(TElement elem);
    virtual void Add(TElement key, TElement elem) {Add(elem);}
    long long int RemoveElem(TElement elem);
    void RemoveInd(long long int del_index);
    virtual void Remove(TElement elem);
    virtual bool GetContainsKey(TElement elem);
    virtual long long int GetCount() {return GetLength();}
    virtual long long int GetCapacity() {return GetLength();}
#ifdef DEBUG_MODE
    TreeNode<TElement>* GetRootPtr();
#endif
private:
    TreeNode<TElement>* root_ = nullptr;
    long long int length_ = 0;
};

template <typename TElement>
TElement BinaryTree<TElement>::Get(TElement elem) {
    if (IndexOf(elem) < 0) {
        string er = "elem not found in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    return elem;
}

template <typename TElement>
bool BinaryTree<TElement>::GetContainsKey(TElement elem) {
    if (IndexOf(elem) < 0) {
        return false;
    }
    return true;
}

template <typename TElement>
void BinaryTree<TElement>::Remove(TElement elem) {
    if (IndexOf(elem) < 0) {
        string er = "element not found in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    RemoveElem(elem);
}

template <typename TElement>
BinaryTree<TElement>::BinaryTree(TElement elem) {
    try {
        root_ = new struct TreeNode<TElement>;
    } catch (std::bad_alloc& ba) {
        std::string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    root_->index_ = 0;
    root_->info_ = elem;
    root_->left_ = root_->right_ = nullptr;
    length_ = 1;
}

template <typename TElement>
BinaryTree<TElement>::~BinaryTree() {
    std::stack<struct TreeNode<TElement>*> st;
    if (root_ != nullptr) {
        st.push(root_);
    }
    
    struct TreeNode<TElement>* tmp;
    while (st.empty() == 0) {
        tmp = st.top();
        st.pop();
        if (tmp->left_ != nullptr) {
            st.push(tmp->left_);
        }
        if (tmp->right_ != nullptr) {
            st.push(tmp->right_);
        }
        delete tmp;
    }
}

template <typename TElement>
long long int BinaryTree<TElement>::GetLength() {
    return length_;
}

template <typename TElement>
int BinaryTree<TElement>::GetIsEmpty() {
    if (length_ == 0) {
        return 1;
    }
    return 0;
}

template <typename TElement>
TElement BinaryTree<TElement>::GetInd(long long int index) {
    if (index < 0) {
        std::string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (index >= length_) {
        std::string er = "index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    TreeNode<TElement>* tmp = root_;
    while (tmp->index_ != index) {
        if (index < tmp->index_) {
            tmp = tmp->left_;
        } else {
            tmp = tmp->right_;
        }
        if (tmp == nullptr) {
            std::string er = "NULL pointer reached in ";
            er += __PRETTY_FUNCTION__;
            throw invalid_argument(er);
        }
    }
    
    return tmp->info_;
}

template <typename TElement>
long long int BinaryTree<TElement>::IndexOf(TElement elem) {
    if (GetIsEmpty() == 1) {
        return -1;
    }
    struct TreeNode<TElement>* tmp = root_;
    while (tmp != nullptr && tmp->info_ != elem) {
        if (elem < tmp->info_) {
            tmp = tmp->left_;
        } else {
            tmp = tmp->right_;
        }
    }
    
    if (tmp == nullptr) {
        return -1;
    }
    
    return tmp->index_;
}

template <typename TElement>
TElement BinaryTree<TElement>::GetFirst() {
    return GetInd(0);
}

template <typename TElement>
TElement BinaryTree<TElement>::GetLast() {
    return GetInd(length_ - 1);
}

template <typename TElement>
BinaryTree<TElement> BinaryTree<TElement>::GetSubsequence(long long int start_index,
                                                          long long int end_index) {
    if (start_index < 0 || end_index < 0) {
        std::string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (start_index > end_index) {
        std::string er = "start_index < end_index in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (start_index >= length_) {
        std::string er = "start_index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    long long int correct_end_index = (end_index < length_) ? end_index : (length_ - 1);
    BinaryTree<TElement> tmp_tree;
    for (long long int i = start_index; i <= correct_end_index; i++) {
        tmp_tree.Add(this->GetInd(i));
    }
    return tmp_tree;
}

template <typename TElement>
void BinaryTree<TElement>::Add(TElement elem) {
    long long int index = 0;
    struct TreeNode<TElement>* tmp = root_;
    struct TreeNode<TElement>** alloc = &root_;
    while (tmp != nullptr) {
        if (elem < tmp->info_) {
            IncrIndex(tmp);
            index = tmp->index_ - 1;
            alloc = &tmp->left_;
            tmp = tmp->left_;
        } else {
            index = tmp->index_ + 1;
            alloc = &tmp->right_;
            tmp = tmp->right_;
        }
    }
    try {
        *alloc = new struct TreeNode<TElement>;
    } catch (std::bad_alloc& ba) {
        std::string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    (*alloc)->info_ = elem;
    (*alloc)->index_ = index;
    (*alloc)->right_ = (*alloc)->left_ = nullptr;
    length_++;
}

/*!
 * @discussion it will place left subtree instead of removed, right subtree will be added to left
 */
template <typename TElement>
void BinaryTree<TElement>::RemoveInd(long long int del_index) {
    if (del_index < 0) {
        std::string er = "index is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    if (del_index >= length_) {
        std::string er = "del_index is too big in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    struct TreeNode<TElement>* tmp = root_;
    struct TreeNode<TElement>** change = &root_;
    
    while (tmp->index_ != del_index) {
        if (del_index < tmp->index_) {
            DecrIndex(tmp);
            change = &tmp->left_;
            tmp = tmp->left_;
        } else {
            change = &tmp->right_;
            tmp = tmp->right_;
        }
        if (tmp == nullptr) {
            std::string er = "NULL pointer reached in ";
            er += __PRETTY_FUNCTION__;
            throw invalid_argument(er);
        }
    }
    DecrIndex(tmp);
    *change = tmp->left_;
    struct TreeNode<TElement>* deep_tmp = *change;
    while (deep_tmp != nullptr) {
        if (tmp->right_->index_ < deep_tmp->index_) {
            IncrIndex(deep_tmp);
            change = &deep_tmp->left_;
            deep_tmp = deep_tmp->left_;
        } else {
            change = &deep_tmp->right_;
            deep_tmp = deep_tmp->right_;
        }
    }
    *change = tmp->right_;
    delete tmp;
    length_--;
}

/*!
 * @discussion index of element to delete is undefined
 * @return -1 if elem is not in the tree; index of deleted element in the other case
 */
template <typename TElement>
long long int BinaryTree<TElement>::RemoveElem(TElement elem){
    long long int index = IndexOf(elem);
    if (index >= 0) {
        RemoveInd(index);
    }
    return index;
}

#ifdef DEBUG_MODE
template <typename TElement>
struct TreeNode<TElement>* BinaryTree<TElement>::GetRootPtr(){
    return root_;
}
#endif


#endif /* tree_h */
