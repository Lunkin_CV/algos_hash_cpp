#ifndef IDictionary_hpp
#define IDictionary_hpp

#include <stdio.h>
#include "../../l0_Sequence/array_sequence.hpp"
#include <random>

//IDs of modes for flag
#define RANDOM 1
#define UPW 2
#define BCKW 4
#define CRCR 8
#define MAX_MODE 8

template <typename TKey, typename TElement>
class IDictionary {
public:
    virtual ~IDictionary() {};
    virtual long long int GetCount() = 0;
    virtual long long int GetCapacity() = 0;
    virtual TElement Get(TKey key) = 0;
    virtual bool GetContainsKey(TKey key) = 0;
    virtual void Add(TKey key, TElement elem) = 0;
    virtual void Remove(TKey key) = 0;
};

/*####################### Utility functions for IDictionary tasking #######################*/

template <typename TKey>
double PrepareDict(ArraySequence<TKey>* raw_data, IDictionary<TKey, TKey>* dict) {
    clock_t begin, end;
    begin = clock();
    for (long long int i = 0; i < raw_data->GetLength(); i++) {
        dict->Add(raw_data->Get(i), raw_data->Get(i));
    }
    end = clock();
    return double(end - begin) / CLOCKS_PER_SEC;
}

/*!
 * @discussion fills in sequence with templates
 */
template <typename SeqType>
void FillIntStr(SeqType* seq, long long int amnt, int mode) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (amnt < 0) {
        string er = "Amount is negative in ";
        er += __PRETTY_FUNCTION__;
        throw out_of_range(er);
    }
    
    //repeat multiplier
    long long int rep = amnt / INT_MAX + 1;
    if (mode == UPW) {
        //upward
        for (long long int i = 0; i < amnt; i++) {
            (*seq).Append(to_string((int)(i / rep)));
        }
    } else if (mode == BCKW){
        //backward
        for (long long int i = amnt; i > 0; i--) {
            (*seq).Append(to_string((int)(i / rep)));
        }
    } else if(mode == CRCR){
        //criss-cross
        rep = (rep - 1) / 2 + 1;
        for (long long int i = amnt / 2; i > 0; i--) {
            (*seq).Append(to_string((int)(i / rep)));
            (*seq).Append(to_string((int)((amnt - i) / rep)));
        }
        if (amnt % 2 == 1) {
            (*seq).Append(to_string(0));
        }
    } else {
        //random
        random_device rd;
        mt19937_64 gen(rd());
        uniform_int_distribution<int> distr;
        for (long long int i = 0; i < amnt; i++) {
            (*seq).Append(to_string(distr(gen)));
        }
    }
}

template <typename TKey>
void SearchDict(IDictionary<TKey, TKey>* dict, TKey key, ostream* str, double prep_time) {
    clock_t begin, end;
    bool exist_result;
    double time_result;
    
    begin = clock();
    exist_result = dict->GetContainsKey(key);
    end = clock();
    time_result = double(end - begin) / CLOCKS_PER_SEC;
    
    if (exist_result == false) {
        cout << "Result: not found.\n";
    } else {
        cout << "Result: found\n";
    }
    cout << "Preparation time: " << prep_time << " seconds.\n";
    cout << "Search time: " << time_result << " seconds.\n";
    cout << "Total time: " << prep_time + time_result << " seconds.\n";
    cout << "\n";
    
    if (str != nullptr) {
        if (exist_result == false) {
            *str << "Result: not found.\n";
        } else {
            *str << "Result: found\n";
        }
        *str << "Preparation time: " << prep_time << " seconds.\n";
        *str << "Search time: " << time_result << " seconds.\n";
        *str << "Total time: " << prep_time + time_result << " seconds.\n";
        *str << "\n";
    }
}


#endif /* IDictionary_hpp */
