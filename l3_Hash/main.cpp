#include <iostream>
#include "task.hpp"
#include <fstream>
#include <sstream>
#include "exec.hpp"

#define HELP "#################### OPERATION MANUAL ####################\n\
This program allows you to compare Hash Table search, Binary Tree search and AVL Tree search on your own sequences of strings (from file or keyboard) and generated sequences (random, upward, backward and criss-cross). \n\
It will generate a report with results and, if you want, save raw sequence.\n\n\
There are two types of keys: major keys and minor keys.\n\
Major key tells the program what to do. In each command you must use only one major key.\n\
Minor key adds details. However, sometimes minor keys are required.\n\
So the general form of a command is:\n\
./l2_Search --major-key --minor-key1 param --minor-key2 --param1,param2,param3\n\n\
There are three types of tasks: auto tasks, file tasks, manual tasks. \n\
Binary search operates with sequences based on either arrays or lists.\n\
The program can generate sequence for search by itself (random, upward, backward and criss-cross).\
Required minor keys: --amount\n\
The program can take sequence from first line of .csv file with ';' delimiter. \
Required minor keys: --raw-path\n\
Manual tasks take sequences from user via terminal. No required minor keys.\n\
\n\
List of major keys with their minor keys:\n\
--help Shows this manual.\n\
--auto Search in generated sequence.\n\
--file Add new file task with sequences based on arrays.\n\
--manual Add new manual task with sequences based on arrays.\n\
    [--algos alg1,alg2,alg3] List of algorythms to test.\n\
        h - hash table search\n\
        t - binary tree search\n\
        a - binary avl tree search\n\
        Default: h,t,a\n\
    {--raw-path PATH} Path to file with sequence.\n\
    [--report-path PATH] Path to file for report.\n\
    [--mode mode] Only for generated sequence. Mode for sequence generation.\n\
        r - random\n\
        u - upward\n\
        b - backward\n\
        c - criss-cross\n\
        Default: r\n\
    --amount am1 Only for generated sequence. Amounts for sequnce generation.\n\
    [--ratio 2.0] Extend/compress ratio for hash table. Must be > 1\n\
    [--rarity 10.0] Rarity for hash table. Must be >= ratio\n\
"


int main(int argc, const char * argv[]) {
    SearchTask<string>* main_task = nullptr;
    ArraySequence<string> seq;
    ofstream report_file;
    int types = 0;
    int mode = 0;
    long long int amount = 0;
    double ratio = 0;
    double rarity = 0;
    try {
        if (argc == 1) {
            cout << "Try --help for instructions.\n";
        } else if (strcmp(argv[1], "--help") == 0) {
            cout << HELP;
        } else if (strcmp(argv[1], "--manual") == 0) {
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq, &ratio, &rarity);
            
            try {
                main_task = new SearchTask<string>(types, ratio, rarity);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(&report_file, main_task);
        } else if (strcmp(argv[1], "--auto") == 0) {
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq, &ratio, &rarity);
            if (amount <= 0) {
                string er = "amount is zero or negative in ";
                er += __PRETTY_FUNCTION__;
                throw invalid_argument(er);
            }
            try {
                main_task = new SearchTask<string>(amount, mode, types, ratio, rarity);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(&report_file, main_task);
        } else if (strcmp(argv[1], "--file") == 0) {
            ArgRec(argc, argv, &types, &report_file, &amount, &mode, &seq, &ratio, &rarity);
            if (seq.GetIsEmpty() == 1) {
                string er = "bad file in ";
                er += __PRETTY_FUNCTION__;
                throw invalid_argument(er);
            }
            try {
                main_task = new SearchTask<string>(&seq, types, ratio, rarity);
            } catch (std::bad_alloc& ba) {
                string er = "Bad memory allocation in ";
                er += __PRETTY_FUNCTION__;
                throw runtime_error(er);
            }
            Exec(&report_file, main_task);
        } else {
            cout << "Command not found. Try --help for instructions.\n";
        }
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }
    return 0;
}
