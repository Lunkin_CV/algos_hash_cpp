#ifndef tree_utility_h
#define tree_utility_h

#include <stdio.h>
#include <exception>

using namespace std;

/*####################### NodeInfo #######################*/
template <typename TKey, typename TValue>
class NodeInfo {
public:
    NodeInfo(TKey key, TValue value);
    NodeInfo(TKey key);
    NodeInfo() {};
    ~NodeInfo() {};
    TKey GetKey() const;
    TValue GetValue() const;
    bool operator==(const NodeInfo<TKey, TValue>& b);
    bool operator!=(const NodeInfo<TKey, TValue>& b);
    bool operator>(const NodeInfo<TKey, TValue>& b);
    bool operator<(const NodeInfo<TKey, TValue>& b);
    bool operator>=(const NodeInfo<TKey, TValue>& b);
    bool operator<=(const NodeInfo<TKey, TValue>& b);
    void operator=(const NodeInfo<TKey, TValue>& b);
private:
    TKey key_;
    TValue value_;
};

template <typename TKey, typename TValue>
NodeInfo<TKey, TValue>::NodeInfo(TKey key, TValue value) {
    key_ = key;
    value_ = value;
}

/*!
 * @discussion in case of creating by key key_==value_
 */
template <typename TKey, typename TValue>
NodeInfo<TKey, TValue>::NodeInfo(TKey key) {
    key_ = key;
    value_ = key;
}

template <typename TKey, typename TValue>
TKey NodeInfo<TKey, TValue>::GetKey() const {
    return key_;
}

template <typename TKey, typename TValue>
TValue NodeInfo<TKey, TValue>::GetValue() const {
    return value_;
}

/*!
 * @discussion relation operators compare only key_.
 * if (key1_ == key2_ && value1_ != value2_) NodeInfo1 == NodeInfo2
 */
template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator==(const NodeInfo<TKey, TValue>& b) {
    if (key_ == b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator!=(const NodeInfo<TKey, TValue>& b) {
    if (key_ != b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator>(const NodeInfo<TKey, TValue>& b) {
    if (key_ > b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator<(const NodeInfo<TKey, TValue>& b) {
    if (key_ < b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator>=(const NodeInfo<TKey, TValue>& b) {
    if (key_ >= b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
bool NodeInfo<TKey, TValue>::operator<=(const NodeInfo<TKey, TValue>& b) {
    if (key_ <= b.GetKey()) {
        return true;
    }
    return false;
}

template <typename TKey, typename TValue>
void NodeInfo<TKey, TValue>::operator=(const NodeInfo<TKey, TValue>& b) {
    key_ = b.GetKey();
    value_ = b.GetValue();
}

/*####################### Index Correction #######################*/
/*!
 * @discussion increments index of node and its right subtree
 */
template <typename TTreeNode>
void IncrIndex(TTreeNode* tree) {
    if (tree == nullptr) {
        std::string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    tree->index_++;
    
    std::stack<TTreeNode*> st;
    if (tree->right_ != nullptr) {
        st.push(tree->right_);
    }
    
    TTreeNode* tmp;
    while (st.empty() == 0) {
        tmp = st.top();
        st.pop();
        if (tmp->left_ != nullptr) {
            st.push(tmp->left_);
        }
        if (tmp->right_ != nullptr) {
            st.push(tmp->right_);
        }
        tmp->index_++;
    }
}

/*!
 * @discussion decrements index of node and its right subtree
 */
template <typename TTreeNode>
void DecrIndex(TTreeNode* tree) {
    if (tree == nullptr) {
        std::string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    tree->index_--;
    
    std::stack<TTreeNode*> st;
    if (tree->right_ != nullptr) {
        st.push(tree->right_);
    }
    
    TTreeNode* tmp;
    while (st.empty() == 0) {
        tmp = st.top();
        st.pop();
        if (tmp->left_ != nullptr) {
            st.push(tmp->left_);
        }
        if (tmp->right_ != nullptr) {
            st.push(tmp->right_);
        }
        tmp->index_--;
    }
}



#endif /* tree_utility_h */
