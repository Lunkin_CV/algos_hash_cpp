#ifndef exec_h
#define exec_h

/*!
 * @discussion exports new set of data from file's line to sequence
 */
template <typename SeqType>
void ImportLine(ifstream* str, SeqType* seq) {
    if (seq == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    
    if (*str) {
        string line, token;
        stringstream iss;
        if (getline(*str, line))
        {
            iss << line;
            while (getline(iss, token, ';'))
            {
                (*seq).Append(token);
            }
        }
    } else {
        string er = "bad_file";
        throw invalid_argument(er);
    }
}

void ArgRec(int argc,
            const char* argv[],
            int* types,
            ofstream* report_file,
            long long int* amount,
            int* mode,
            ArraySequence<string>* seq,
            double* ratio,
            double* rarity){
    for (int i = 2; i < argc - 1; i++) {
        if (strcmp(argv[i], "--report-path") == 0) {
            if (i + 1 < argc) {
                (*report_file).open(argv[i + 1], ios::out | ios::ate);
            }
        } else if (strcmp(argv[i], "--algos") == 0) {
            if (strstr(argv[i + 1], "h") != nullptr) {
                *types |= HASH;
            }
            if (strstr(argv[i + 1], "t") != nullptr) {
                *types |= TREE;
            }
            if (strstr(argv[i + 1], "a") != nullptr) {
                *types |= AVL;
            }
        } else if (strcmp(argv[i], "--mode") == 0) {
            if (strcmp(argv[i + 1], "r") == 0) {
                *mode = RANDOM;
            } else if (strcmp(argv[i + 1], "u") == 0) {
                *mode = UPW;
            } else if (strcmp(argv[i + 1], "b") == 0) {
                *mode = BCKW;
            }else if (strcmp(argv[i + 1], "c") == 0) {
                *mode = CRCR;
            }
        } else if (strcmp(argv[i], "--amount") == 0) {
            *amount = stoll(argv[i + 1]);
        } else if (strcmp(argv[i], "--ratio") == 0) {
            *ratio = stod(argv[i + 1]);
        } else if (strcmp(argv[i], "--rarity") == 0) {
            *rarity = stod(argv[i + 1]);
        } else if (strcmp(argv[i], "--raw-path") == 0) {
            ifstream input;
            input.open(argv[i + 1], ios::in);
            if (input) {
                ImportLine(&input, seq);
            }
        }
    }
    if (*types <= 0) {
        *types = AVL | TREE | HASH;
    }
    if (*ratio <= 1 || *rarity < *ratio) {
        *rarity = 10;
        *ratio = 2;
    }
}

void Exec(ostream* str, SearchTask<string>* task) {
    if (*str) {
        task->PrintRaw(str);
    }
    cout << "Now start to input elements for search.\nAfter each new member press Enter.\nWhen you finish, type \"!end\" and press Enter.\n";
    string line;
    cout << ">";
    while (getline(cin, line)) {
        if (line.compare("!end") == 0) {
            break;
        } else if (line.compare("") == 0) {
            continue;
        }
        task->Search(line, str);
        cout << ">";
    }
}

#endif /* exec_h */
