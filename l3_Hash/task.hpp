#ifndef task_hpp
#define task_hpp

#include <stdio.h>
#include "hash.hpp"
#include "tree.hpp"
#include "avl_tree.hpp"
#include <random>
#include "../../l0_Sequence/array_sequence.hpp"

//IDs of methods
#define HASH 1
#define TREE 2
#define AVL 4
#define MAX_METHOD 4




/*####################### SearchTask #######################*/
template <typename TKey>
class SearchTask {
public:
    SearchTask(ArraySequence<TKey>* raw, int methods, double ratio, double rarity); //from existing sequence
    SearchTask(int methods, double ratio, double rarity); //from user
    SearchTask(long long int ammount, int gen_mode, int methods, double ratio, double rarity); //generate
    SearchTask(long long int ammount, int methods, double ratio, double rarity); //generate random
    ~SearchTask();
    void Search(TKey key, ostream* str);
    void PrintRaw(ostream* str);
    int GetMethods();
private:
    void Prepare();
    
    ArraySequence<TKey>* raw_data_ = nullptr;
    
    HashTable<TKey, TKey>* hash_table_ = nullptr;
    BinaryTree<TKey>* tree_ = nullptr;
    AVLBinaryTree<TKey>* avl_tree_ = nullptr;
    
    double hash_table_prep_time_ = -1;
    double tree_prep_time_ = -1;
    double avl_tree_prep_time_ = -1;
    
    int gen_mode_ = 0;
    int methods_ = 0;
    bool prepared_ = false;
    
};

template <typename TKey>
SearchTask<TKey>::SearchTask(ArraySequence<TKey>* raw, int methods, double ratio, double rarity) {
    if (methods <= 0 || methods >= MAX_METHOD * 2) {
        string er = "invalid methods in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (raw == nullptr) {
        string er = "NULL pointer reached in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (raw->GetIsEmpty() == 1) {
        string er = "raw sequence is empty in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    try {
        raw_data_ = new ArraySequence<TKey>();
        hash_table_ = new HashTable<TKey, TKey>(rarity, ratio);
        tree_ = new BinaryTree<TKey>();
        avl_tree_ = new AVLBinaryTree<TKey>();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    for (long long int i = 0; i < raw->GetLength(); i++) {
        raw_data_->Append(raw->Get(i));
    }
    methods_ = methods;
    Prepare();
}

template<typename TKey>
SearchTask<TKey>::SearchTask(int methods, double ratio, double rarity) {
    if (methods <= 0 || methods >= MAX_METHOD * 2) {
        cout << methods;
        string er = "invalid methods in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    cout << "Now start to input sequence.\nAfter each new member press Enter.\nWhen you finish, type \"end\" and press Enter.\n";
    try {
        raw_data_ = new ArraySequence<TKey>();
        hash_table_ = new HashTable<TKey, TKey>(rarity, ratio);
        tree_ = new BinaryTree<TKey>();
        avl_tree_ = new AVLBinaryTree<TKey>();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    string line;
    cout << ">";
    while (getline(cin, line)) {
        if (line.compare("!end") == 0) {
            break;
        }
        raw_data_->Append(line);
        cout << ">";
    }
    methods_ = methods;
    Prepare();
}

template<typename TKey>
SearchTask<TKey>::SearchTask(long long int ammount, int mode, int methods, double ratio, double rarity) {
    if (methods <= 0 || methods >= MAX_METHOD * 2) {
        string er = "invalid methods in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (ammount <= 0) {
        string er = "ammount is zero or less in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    try {
        raw_data_ = new ArraySequence<TKey>();
        hash_table_ = new HashTable<TKey, TKey>(rarity, ratio);
        tree_ = new BinaryTree<TKey>();
        avl_tree_ = new AVLBinaryTree<TKey>();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    FillIntStr(raw_data_, ammount, mode);
    methods_ = methods;
    Prepare();
    if (mode > MAX_MODE || mode <= 0) {
        gen_mode_ = RANDOM;
    } else {
        gen_mode_ = mode;
    }
}

template<typename TKey>
SearchTask<TKey>::SearchTask(long long int ammount, int methods, double ratio, double rarity) {
    if (methods <= 0 || methods >= MAX_METHOD * 2) {
        string er = "invalid methods in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    if (ammount <= 0) {
        string er = "ammount is zero or less in ";
        er += __PRETTY_FUNCTION__;
        throw invalid_argument(er);
    }
    try {
        raw_data_ = new ArraySequence<TKey>();
        hash_table_ = new HashTable<TKey, TKey>(rarity, ratio);
        tree_ = new BinaryTree<TKey>();
        avl_tree_ = new AVLBinaryTree<TKey>();
    } catch (std::bad_alloc& ba) {
        string er = "Bad memory allocation in ";
        er += __PRETTY_FUNCTION__;
        throw runtime_error(er);
    }
    FillIntStr(raw_data_, ammount, RANDOM);
    methods_ = methods;
    Prepare();
    gen_mode_ = RANDOM;
}

template <typename TKey>
SearchTask<TKey>::~SearchTask() {
    if (raw_data_ != nullptr) {
        delete raw_data_;
    }
    if (hash_table_ != nullptr) {
        delete hash_table_;
    }
    if (tree_ != nullptr) {
        delete tree_;
    }
    if (avl_tree_ != nullptr) {
        delete avl_tree_;
    }
}

template <typename TKey>
void SearchTask<TKey>::Search(TKey key, ostream* str) {
    if (prepared_ == false) {
        cout << "Task is unprepared.\n";
        return;
    }
    
    cout << "#################### Search element " << key << " ####################\n";
    if (str != nullptr) {
        *str << "#################### Search element " << key << " ####################\n";
    }
    
    if ((methods_ & HASH) != 0) {
        cout << "Search in Hash Table.\n";
        if (str != nullptr) {
            *str << "Search in Hash Table.\n";
        }
        SearchDict(hash_table_, key, str, hash_table_prep_time_);
    }
    
    if ((methods_ & TREE) != 0) {
        cout << "Search in Binary Tree.\n";
        if (str != nullptr) {
            *str << "Search in Binary Tree.\n";
        }
        SearchDict(tree_, key, str, tree_prep_time_);
    }
    
    if ((methods_ & AVL) != 0) {
        cout << "Search in AVL Binary Tree.\n";
        if (str != nullptr) {
            *str << "Search in AVL Binary Tree.\n";
        }
        SearchDict(avl_tree_, key, str, avl_tree_prep_time_);
    }
    
    cout << "#################### Search ended ####################\n";
    if (str != nullptr) {
        *str << "#################### Search ended ####################\n";
    }
}

template <typename TKey>
void SearchTask<TKey>::PrintRaw(ostream* str) {
    if (str != nullptr && prepared_ == true) {
        for (long long int i = 0; i < raw_data_->GetLength(); i++) {
            *str << raw_data_->Get(i) << ";";
        }
        *str << "\n";
    }
}

template <typename TKey>
int SearchTask<TKey>::GetMethods() {
    return methods_;
}

template <typename TKey>
void SearchTask<TKey>::Prepare() {
    if (raw_data_->GetIsEmpty() == 1 || prepared_ == true) {
        return;
    }
    
    if ((methods_ & HASH) != 0) {
        hash_table_prep_time_ = PrepareDict(raw_data_, hash_table_);
    }
    
    if ((methods_ & TREE) != 0) {
        tree_prep_time_ = PrepareDict(raw_data_, tree_);
    }
    
    if ((methods_ & AVL) != 0) {
        avl_tree_prep_time_ = PrepareDict(raw_data_, avl_tree_);
    }
    
    prepared_ = true;
}


#endif /* task_hpp */
