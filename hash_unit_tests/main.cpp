#include <iostream>
#include <string>
#include "hash.hpp"
#include "tree.hpp"
#include "avl_tree.hpp"
#include "IDictionary.hpp"

using namespace std;

void InterfaceCheck(IDictionary<string, string>* object) {
    int correct;
    
    //1
    cout << "1. GetContains \"abc\" test.";
    if (object->GetContainsKey("abc") != false) {
        cout << " NOT PASSED\n";
        throw runtime_error("1. GetContains is wrong.");
    }
    cout << " PASSED\n";
    
    //2
    cout << "2. Add \"abc\" test.";
    object->Add("abc", "abc");
    if (object->GetCount() != 1) {
        cout << " NOT PASSED\n";
        throw runtime_error("2. Add is wrong.");
    }
    cout << " PASSED\n";
    
    //3
    cout << "3. GetContains \"abc\" test.";
    if (object->GetContainsKey("abc") != true) {
        cout << " NOT PASSED\n";
        throw runtime_error("3. GetContains or Add is wrong.");
    }
    cout << " PASSED\n";
    
    //4
    cout << "4. GetContains \"cbc\" test.";
    if (object->GetContainsKey("cbc") != false) {
        cout << " NOT PASSED\n";
        throw runtime_error("4. GetContains is wrong.");
    }
    cout << " PASSED\n";
    
    //5
    cout << "5. Get \"abc\" test.";
    if (object->Get("abc") != "abc") {
        cout << " NOT PASSED\n";
        throw runtime_error("5. Get is wrong.");
    }
    cout << " PASSED\n";
    
    //6
    cout << "6. Get unexisting \"cbc\" test.";
    try {
        correct = 0;
        object->Get("cbc");
    } catch (const std::runtime_error& excp) {
        correct = 1;
    }
    if (correct == 0) {
        cout << " NOT PASSED\n";
        throw runtime_error("6. Get is wrong.");
    }
    cout << " PASSED\n";
    
    //7
    cout << "7. Remove unexisting \"cbc\" test.";
    try {
        correct = 0;
        object->Remove("cbc");
    } catch (const std::runtime_error& excp) {
        correct = 1;
    }
    if (correct == 0) {
        cout << " NOT PASSED\n";
        throw runtime_error("7. Remove is wrong.");
    }
    cout << " PASSED\n";
    
    //8
    cout << "8. Remove \"abc\" test.";
    object->Remove("abc");
    if (object->GetCount() != 0 ||
        object->GetContainsKey("abc") != false) {
        cout << " NOT PASSED\n";
        throw runtime_error("8. Remove is wrong.");
    }
    cout << " PASSED\n";
}

int main(int argc, const char * argv[]) {
    
    try {
        cout << "#################### Tests for Hash function. ####################\n";
        
        std::string test_st;
        int correct;
        
        
        //1
        cout << "1. Blank key.";
        if (GetHashCode(test_st) != 0) {
            cout << " NOT PASSED\n";
            throw runtime_error("1. GetHashCode() is wrong.");
        }
        cout << " PASSED\n";
        
        //2
        cout << "2. Hash feature test. key = abc.";
        test_st = "abc";
        if (GetHashCode(test_st) != GetHashCode(test_st)) {
            cout << " NOT PASSED\n";
            throw runtime_error("2. GetHashCode() is wrong.");
        }
        cout << " PASSED\n";
        
        cout << "#################### Tests for Hash table. ####################\n";
        
        HashTable<string, int>* test_table;
        
        //1
        cout << "1. Constructor test. rarity = 4, ratio = 2.";
        try {
            test_table = new HashTable<string, int>(4, 2);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        if (test_table->GetCapacity() != 5 ||
            test_table->GetCount() != 0 ||
            test_table->GetRatio() != 2 ||
            test_table->GetRarity() != 4) {
            cout << " NOT PASSED\n";
            throw runtime_error("1. Constructor of hash table is wrong.");
        }
        cout << " PASSED\n";
        
        //2
        cout << "2. Add \"abc\" test.";
        test_table->Add("abc", 15);
        if (test_table->GetCapacity() != 5 ||
            test_table->GetCount() != 1) {
            cout << " NOT PASSED\n";
            throw runtime_error("2. Add is wrong.");
        }
        cout << " PASSED\n";
        
        //3
        cout << "3. GetContains \"abc\" test.";
        if (test_table->GetContainsKey("abc") != true) {
            cout << " NOT PASSED\n";
            throw runtime_error("3. GetContains or Add is wrong.");
        }
        cout << " PASSED\n";
        
        //4
        cout << "4. GetContains \"cbc\" test.";
        if (test_table->GetContainsKey("cbc") != false) {
            cout << " NOT PASSED\n";
            throw runtime_error("4. GetContains is wrong.");
        }
        cout << " PASSED\n";
        
        //5
        cout << "5. Get \"abc\" test.";
        if (test_table->Get("abc") != 15) {
            cout << " NOT PASSED\n";
            throw runtime_error("5. Get is wrong.");
        }
        cout << " PASSED\n";
        
        //6
        cout << "6. Get unexisting \"cbc\" test.";
        try {
            correct = 0;
            test_table->Get("cbc");
        } catch (const std::runtime_error& excp) {
            correct = 1;
        }
        if (correct == 0) {
            cout << " NOT PASSED\n";
            throw runtime_error("6. Get is wrong.");
        }
        cout << " PASSED\n";
        
        //7
        cout << "7. Remove unexisting \"cbc\" test.";
        try {
            correct = 0;
            test_table->Remove("cbc");
        } catch (const std::runtime_error& excp) {
            correct = 1;
        }
        if (correct == 0) {
            cout << " NOT PASSED\n";
            throw runtime_error("7. Remove is wrong.");
        }
        cout << " PASSED\n";
        
        //8
        cout << "8. Remove \"abc\" test.";
        test_table->Remove("abc");
        if (test_table->GetCount() != 0 ||
            test_table->GetCapacity() != 2 ||
            test_table->GetContainsKey("abc") != false) {
            cout << " NOT PASSED\n";
            throw runtime_error("8. Remove is wrong.");
        }
        cout << " PASSED\n";
        
        //9
        cout << "9. Extend test.";
        test_table->Add("abc", 1);
        test_table->Add("abc", 2);
        test_table->Add("abc", 3);
        if (test_table->GetCount() != 3 ||
            test_table->GetCapacity() != 4 ||
            test_table->GetContainsKey("abc") != true) {
            cout << " NOT PASSED\n";
            throw runtime_error("9. Extend is wrong.");
        }
        cout << " PASSED\n";
        
        //10
        cout << "10. Compress test.";
        test_table->Remove("abc");
        test_table->Remove("abc");
        if (test_table->GetCount() != 1 ||
            test_table->GetCapacity() != 2 ||
            test_table->GetContainsKey("abc") != true) {
            cout << " NOT PASSED\n";
            throw runtime_error("10. Compress is wrong.");
        }
        cout << " PASSED\n";
        
        cout << "#################### Interface Tests for Hash table. ####################\n";
        HashTable<string, string>* another_test_table;
        try {
            another_test_table = new HashTable<string, string>(4, 2);
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        InterfaceCheck(another_test_table);
        delete another_test_table;
        
        cout << "#################### Interface Tests for Binary tree. ####################\n";
        BinaryTree<string>* test_tree;
        try {
            test_tree = new BinaryTree<string>();
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        InterfaceCheck(test_tree);
        delete test_tree;
        
        cout << "#################### Interface Tests for AVLBinary tree. ####################\n";
        AVLBinaryTree<string>* test_avl_tree;
        try {
            test_avl_tree = new AVLBinaryTree<string>();
        } catch (std::bad_alloc& ba) {
            string er = "Bad memory allocation in ";
            er += __PRETTY_FUNCTION__;
            throw runtime_error(er);
        }
        InterfaceCheck(test_avl_tree);
        delete test_avl_tree;

        
        cout << "\nALL TESTS ARE PASSED\n";
    } catch (const runtime_error& re) {
        cout << "Runtime error: " << re.what() << endl;
    } catch (const exception& ex) {
        cout << "Error occurred: " << ex.what() << endl;
    } catch (...) {
        cout << "Unknown failure occurred. Possible memory corruption" << endl;
    }
    return 0;
}
